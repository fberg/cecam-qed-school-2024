{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "2ce23647",
   "metadata": {},
   "source": [
    "# Optical spectra from the electron-photon sternheimer equation \n",
    "[Link to tutorial](https://www.octopus-code.org/documentation/14/tutorial/response/optical_spectra_from_sternheimer/)\n",
    "\n",
    "In this tutorial we will learn how to calculate the absorption spectrum of a molecule strongly coupled to a photon mode within a cavity using the Sternheimer approach formulated within the QEDFT framework. This approach is a natural extension to the electron-only [Sternheimer equation](https://www.octopus-code.org/documentation/14/tutorial/response/optical_spectra_from_sternheimer/). The main aim in this tutorial will be to capture the hallmark of strong light-matter coupling (i.e. the emergence of a Rabi splitting between polaritons) usually identified in linear spectroscopy. For a benzene molecule confined within a cavity, we will investigate the resonant coupling of a cavity mode to the $\\pi-\\pi^{*}$ excitation that occurs around $\\sim 7$ eV and capture the emergence of polaritons in the absorption spectrum.\n",
    "\n",
    "The uncoupled Sternheimer approach is also known as density-functional perturbation theory. The approach has superior scaling, is more efficient for dense spectra, and is more applicable to nonlinear response. One disadvantage is that one needs to proceed one frequency point at a time, rather than getting the whole spectrum at once. This disadvantage is normally circumvented using parallel architectures since the frequency-dependent Sternheimer equation parallelizes naturally as the responses at different frequencies can be computed independently of each other.\n",
    "\n",
    "\n",
    "# The electron-photon sternheimer approach\n",
    "\n",
    "The frequency-dependent Sternheimer approach formulated within the framework of QEDFT, determines the correlated electron-photon density and photon displacement coordinate responses in terms of the occupied ground-state Kohn-Sham orbitals $\\varphi_{k}(\\textbf{r})$ and linear-response $\\varphi_{k}^{(\\pm)}(\\textbf{r},\\omega)$ given by\n",
    "\n",
    "$$\n",
    "\\delta n(\\textbf{r},\\omega) \n",
    "= \\sum_{k=1}^{N_{e}} \\left[ \\varphi_{k}^{*}(\\textbf{r}) \\varphi_{k}^{(+)}(\\textbf{r},\\omega) +  \\varphi_{k}(\\textbf{r}) \\left[\\varphi_{k}^{(-)}(\\textbf{r},\\omega)\\right]^{*}\\right] , \\qquad \\textrm{and} \\qquad\n",
    "\\delta q_{\\alpha}(\\omega) = \\delta q_{\\alpha}^{(+)}(\\omega) + \\delta q_{\\alpha}^{(-)}(\\omega)\n",
    "$$\n",
    "\n",
    "by solving the self-consistent linear coupled Sternheimer equations\n",
    "$$\n",
    "\\left(\\omega - \\hat{h} + \\epsilon_{k} + i\\eta\\right)\\varphi_{k}^{(+)}(\\textbf{r},\\omega) =   \\delta v_{\\textrm{KS}}(\\textbf{r},\\omega) \\varphi_{k}(\\textbf{r}) \\, ,  \\qquad \\textrm{and} \\qquad\n",
    "\\left(\\omega + \\hat{h} - \\epsilon_{k} + i\\eta\\right)\\varphi_{k}^{(-)}(\\textbf{r},\\omega) = -  \\delta v_{\\textrm{KS}}(\\textbf{r},\\omega) \\varphi_{k}^{*}(\\textbf{r}) \\, , \n",
    "$$\n",
    "\n",
    "$$\n",
    "\\delta q_{\\alpha,v}^{(+)}(\\omega) \n",
    "= \\frac{1}{2\\omega_{\\alpha}^{2}} \\left(\\frac{1}{\\omega - \\omega_{\\alpha} + i\\eta'} \\right) \\int d^{3}\\textbf{r}' g_{M}^{n_{\\alpha}}(\\textbf{r}') \\delta n(\\textbf{r}',\\omega) \\, , \\qquad \\textrm{and} \\qquad\n",
    "\\delta q_{\\alpha,v}^{(-)}(\\omega) \n",
    "= -\\frac{1}{2\\omega_{\\alpha}^{2}} \\left( \\frac{1}{\\omega + \\omega_{\\alpha} + i\\eta'}\\right) \\int d^{3}\\textbf{r}' g_{M}^{n_{\\alpha}}(\\textbf{r}') \\delta n_{v}(\\textbf{r}',\\omega) \\, .\n",
    "$$\n",
    "\n",
    "Here, $\\hat{h}$ is the Kohn-Sham Hamiltonian, $\\epsilon_{k}$ are the eigenvalues of the ground-state, $\\omega_{\\alpha}$ is the photon mode frequencies, and the response of the Kohn-Sham potential is given explicitly as\n",
    "\n",
    "$$\n",
    "\\delta v_{\\textrm{KS}}(\\textbf{r},\\omega) = \\delta v(\\textbf{r},\\omega) + \\int d^{3}\\textbf{r}' f_{\\textrm{Mxc}}^{n}(\\textbf{r},\\textbf{r}',\\omega) \\delta n(\\textbf{r}',\\omega) + \\sum_{\\alpha} f_{\\textrm{Mxc}}^{q_{\\alpha}}(\\textbf{r},\\omega)\\delta  q_{\\alpha}(\\omega)\n",
    "$$\n",
    "\n",
    "The frequency-dependnent term $f^n_\\text{Mxc}=f^n_\\text{Hxc} + f^n_\\text{pxc}$ is the mean-field exhange-correlation kernel which is a sum of the Hartree exchange-correlation kernel $f^n_\\text{Hxc}$ and electron-photon exchange-correlation $f^n_\\text{pxc}$ kernel. The terms $f^{q_\\alpha}_\\text{pxc}$ and $g^{n_{\\alpha}}_{\\text{M}}$ are also electron-photon exchange-correlation kernels that account for electron-photon interactions.\n",
    "\n",
    "As a last remark, we note that in the decoupling limit between light and matter (i.e. when the light-matter coupling $\\boldsymbol{\\lambda}_{\\alpha} \\rightarrow 0$), the photon dispalcement field $q_{\\alpha}(\\omega)$ decouples and electron-photon Sternheimer equations simplifies to the electron-only Sterneheimer equation.\n",
    "\n",
    "\n",
    "\n",
    "## References\n",
    "For details about time-dependent QEDFT, refer to the following:\n",
    "\n",
    "[1] Davis M. Welakuh, [Ab initio Strong Light-Matter Theoretical Framework for Phenomena in Non-relativistic Quantum Electrodynamics](https://ediss.sub.uni-hamburg.de/handle/ediss/9069).\n",
    "\n",
    "[2] Davis M. Welakuh, J. Flick et al., *Frequency-Dependent Sternheimer Linear-Response Formalism for\n",
    "Strongly Coupled Light−Matter Systems* [J. Chem. Theory Comput. 2022, 18, 4354−4365](https://doi.org/10.1021/acs.jctc.2c00076).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "00e720f2",
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import matplotlib.gridspec as gridspec"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cc4e03e7",
   "metadata": {},
   "source": [
    "## Ground-state\n",
    "\n",
    "Our starting point will be the calculation of the uncoupled electronic ground state to obtain the occupied Kohn-Sham orbitals and the ground-state electronic density. We will use the following input file for the calculation:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "051b5ff8",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "#stdout = 'stdout_gs.txt'\n",
    "#stderr = 'stderr_gs.txt'\n",
    "\n",
    "CalculationMode = gs\n",
    "#UnitsOutput = eV_angstrom\n",
    "\n",
    "BoxShape = minimum\n",
    "Radius = 4.0*angstrom\n",
    "Spacing = 0.20*angstrom\n",
    "\n",
    "XYZCoordinates = \"benzene.xyz\"\n",
    "\n",
    "ConvRelDens = 1e-8"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c9223a76",
   "metadata": {},
   "source": [
    "Now, we can run **octopus** with the following line to obtain the ground-state properties of the single benzene molecule."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "814e3358",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash \n",
    "export OMP_NUM_THREADS=1\n",
    "mpirun -x OMP_NUM_THREADS -np 4 octopus > out_gs.log"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0d47bf4b",
   "metadata": {},
   "source": [
    "Note that for the benzene molecule 30 valence electrons are explicitly considered amounting to 15 occupied orbitals written in the directory `restart/gs`, while the core atoms are considered implicitly by LDA Troullier-Martins pseudopotentials. Note that the variable [UnitsOutput](https://octopus-code.org/documentation/13/variables/execution/units/unitsoutput/) is commented out because in this tutorial we want to work in atomic units (au). You can take a look at the eigenvalues of the occupied states in the file `static/info`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9830d7c3-d3ba-4737-86bd-c9d62aa63e3e",
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat static/info"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fb2b5008-f8d4-43c2-bc4a-f134ba1b3c8c",
   "metadata": {},
   "source": [
    "An advantage that the QEDFT Sterhheimer approach has over the electron-photon Casida approach is that requires only the occupied orbitals and no unoccupied orbitals."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4d628d27-c4a0-401b-a8fd-20850b46a4e0",
   "metadata": {},
   "source": [
    "##  Sterheimer calculation (electron-only)\n",
    "\n",
    "First we compute the required data for the absorption spectrum of the uncoupled molecule by performing solving the electron-only Sterneimer equation formulated with TDDFT. This is necessary when we want to investigate the Rabi splitting of a specific excitation without knowing *apriori* $\\;$ the details of the molecule's free-space absorption spectra. Also, we do this calculation to later compare to the results from the electron-photon Sternheimer calculation that hightlights the emergence of the Rabi splitting of the $\\pi-\\pi^{*}$ excitation.\n",
    "\n",
    "To calculate the absorption of the benzene molecule in free-space, we now modify the [CalculationMode](https://www.octopus-code.org/documentation//13/variables/calculation_modes/calculationmode) from `gs` to `em_resp` and add the lines bewlow `ConvRelDens` as shown in the following inp file:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7440405f",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "#stdout = 'stdout_em_resp.txt'\n",
    "#stderr = 'stderr_em_resp.txt'\n",
    "\n",
    "CalculationMode = em_resp\n",
    "#UnitsOutput = eV_angstrom\n",
    "\n",
    "BoxShape = minimum\n",
    "Radius = 4.0*angstrom\n",
    "Spacing = 0.20*angstrom\n",
    "\n",
    "XYZCoordinates = \"benzene.xyz\"\n",
    "\n",
    "ConvRelDens = 1e-8\n",
    "\n",
    "%EMFreqs\n",
    " 50 | 0.0 | 0.7\n",
    "%\n",
    "\n",
    "EMEta = 0.003675\n",
    "\n",
    "Preconditioner = no\n",
    "LinearSolver = qmr_dotp\n",
    "ExperimentalFeatures = yes"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "791f8f98",
   "metadata": {},
   "source": [
    "The frequencies of interest must be specified, and we choose them based on the regiron of the spectrum we are interested in. For example, the block `EMFreqs` specifies 50 frequencies spanning the range $0$ to $0.7$ Hartree with equidistant frequencies spacing $0.0142$ Hartree that covers a frequency that the $\\pi-\\pi^{*}$ of benzene lies within. If we didn’t know where to look, then looking at a coarse frequency grid and then sampling more points in the region that seems to have a peak (including looking for signs of resonances in the real part of the polarizability) would be a reasonable approach. The variable `EMEta` represents the parameter $\\eta$ that appears in the Sternheimer equations and this imaginary part is usually small in order to be able to obtain the imaginary part of the response, and to avoid divergence at resonances. The resonances are broadened into Lorentzians with this width. The larger the $\\eta$ paramter, the easier the SCF convergence is, but the lower the resolution of the spectrum. To help in the numerical solution, we turn off the preconditioner `Preconditioner = no` (which sometimes causes trouble here), and use a linear solver (i.e. `LinearSolver = qmr_dotp`) which is experimental but will give convergence much faster than the default one.\n",
    "\n",
    "Now, we can run **octopus** with the following line to obtain the frequency linear-response properties of the single benzene molecule."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "84efa894",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash \n",
    "export OMP_NUM_THREADS=1\n",
    "mpirun -x OMP_NUM_THREADS -np 4 octopus > out_el_only.log"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a6003b84-0deb-42d8-9413-0c77d018e8c8",
   "metadata": {},
   "source": [
    "A new directory will be created named `em_resp` which contain several subdirectories `em_resp/freq_*` that has information of the spectra for the different discretized frequencies. Rename the `em_resp` directory to `em_resp_el` to indicate it is an electron-only data with the following command:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "67453b81-0ffe-47b1-9a32-d86988a2393c",
   "metadata": {
    "vscode": {
     "languageId": "plaintext"
    }
   },
   "outputs": [],
   "source": [
    "!mv em_resp em_resp_el"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b89d87f8",
   "metadata": {},
   "source": [
    "\n",
    "##  The QEDFT Sternheimer Calculation\n",
    "\n",
    "We are now set to calculate the optical spectrum of a benzene molecule where a single photon mode is resonantly coupled to the $\\pi-\\pi^{*}$ transition energy. We assume that we have already obtained required results from the TDDFT calculation which is necessary for the QEDFT calculation. This means we have deduced the $\\pi-\\pi^{*}$ transition energy of $6.936$ eV ($0.2549$ Hartree) from the TDDFT Sternheimer calculation. Note that this can be also deduced from the electron-only Casida or real-time TDDFT approaches.\n",
    "\n",
    "Now keep the [CalculationMode](https://www.octopus-code.org/documentation//13/variables/calculation_modes/calculationmode) set to `em_resp` for the QEDFT Sternheimer calculations and add the lines below `ExperimentalFeatures` in the inp file.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "62edb0c0-d78d-4e77-9ed5-86ce70ec4f0d",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "#stdout = 'stdout_em_resp.txt'\n",
    "#stderr = 'stderr_em_resp.txt'\n",
    "\n",
    "CalculationMode = em_resp\n",
    "#UnitsOutput = eV_angstrom\n",
    "\n",
    "BoxShape = minimum\n",
    "Radius = 4.0*angstrom\n",
    "Spacing = 0.20*angstrom\n",
    "\n",
    "XYZCoordinates = \"benzene.xyz\"\n",
    "\n",
    "ConvRelDens = 1e-8\n",
    "\n",
    "%EMFreqs\n",
    " 500 | 0.0 | 0.7\n",
    "%\n",
    "\n",
    "EMEta = 0.003675\n",
    "\n",
    "Preconditioner = no\n",
    "LinearSolver = qmr_dotp\n",
    "ExperimentalFeatures = yes\n",
    "\n",
    "EnablePhotons = yes\n",
    "ElectronPhotonEta = 0.00003675\n",
    "\n",
    "%PhotonModes\n",
    " 0.254911257 | 0.01 | 1 | 0 | 0\n",
    "%"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "382251e8-cea1-401f-b58e-e992ea68ced5",
   "metadata": {},
   "source": [
    "The variable `EnablePhotons = yes` instructs octopus to enable the coupling to photons and the `ExperimentalFeatures = yes` must be set since the electron-photon setup is a relatively new feature. The [PhotonModes](https://www.octopus-code.org/documentation/main/variables/hamiltonian/xc/photonmodes/) block takes input variables in the following structure: $|\\omega_{\\alpha}|\\lambda_{\\alpha}|\\textbf{e}_{x}|\\textbf{e}_{y}|\\textbf{e}_{z}|$. For the above input file it means the cavity mode has a frequency $\\omega_{\\alpha}=0.255$ Hartree, the light-matter coupling has a strength $\\lambda_{\\alpha}=0.01$ au, and the photon mode is polarized only in the $x$-direction. The parameter `ElectronPhotonEta` represents the broadening $\\eta'$ in the photonic sector of the electron-photon Sternheimer equation necessary to avoid divergence at the cavity frequency $\\omega_{\\alpha}$.\n",
    "\n",
    "Now, rerun **octopus** to calculate linear-response properties of the coupled light-matter system. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d3d25b61-07bf-48a2-85e8-850d774bb3b7",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash \n",
    "export OMP_NUM_THREADS=1\n",
    "mpirun -x OMP_NUM_THREADS -np 4 octopus > out_el_ph.log"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5301a526-ca3b-46fb-a7a6-8745eb6b0742",
   "metadata": {},
   "source": [
    "A new directory will be created again named `em_resp` which has several subdirectories `em_resp/freq_*` that content information of the spectra for the different discretized frequencies for the coupled light-matter system. Rename the `em_resp` directory to `em_resp_el_pt` to indicate it is an electron-photon data with the following command:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "980a6213-f9e1-4362-9c1d-7207a530b3dd",
   "metadata": {},
   "outputs": [],
   "source": [
    "!mv em_resp em_resp_el_pt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e833da6f-506b-4adc-917a-1a4daea87e72",
   "metadata": {},
   "source": [
    "The renamed directory `em_resp_el_pt` also contains subdirectories for each frequency calculated, each in turn containing `EMEta` (listing $\\eta = 0.003675$ Hartree), alpha (containing the real part of the polarizability tensor), and cross_section (containing the cross-section for absorption, based on the imaginary part of the polarizability). For example, you can take a look at one of the computed frequencies by executing the following line"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "46f3fa00-4dbe-48c9-8e45-b1118841cab1",
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat em_resp_el_pt/freq_0.0000/alpha"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ff2075f9-d406-49ef-9a43-9913fa692c61",
   "metadata": {},
   "source": [
    "##  Absorption spectrum\n",
    "\n",
    "The leading diagonal of the `Polarizability tensor` list from top to bottom the tensor elements $\\alpha_{xx}$, $\\alpha_{yy}$, $\\alpha_{zz}$ and the `Isotropic average` is $\\frac{1}{3}(\\alpha_{xx} + \\alpha_{yy} + \\alpha_{zz})$. In this tutorial, we will use only the $\\alpha_{xx}$ component for each computed frequencies since the photon mode of the cavity is polarized along the $x$-direction. The cross-section is related to the strength function by $S \\left( \\omega \\right) = \\frac{mc}{2 \\pi^2 \\hbar^2} \\sigma \\left( \\omega \\right)$ and the optical absorption cross-section is related to the dynamic polarizability via $\\sigma \\left( \\omega \\right) = \\frac{4 \\pi \\omega}{c} \\mathrm{Im}\\ \\alpha \\left( \\omega \\right) $.\n",
    "\n",
    "\n",
    "Run the following python script to extract and combine all the required results to a single file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "32318f46",
   "metadata": {},
   "outputs": [],
   "source": [
    "# List directories matching 'em_resp/freq*'\n",
    "\n",
    "c0 = 137.0\n",
    "\n",
    "fd_el = open('data_computed_spectra_el.txt', 'w')\n",
    "fd_el_pt = open('data_computed_spectra_el.txt', 'w')\n",
    "\n",
    "el_dir = os.listdir(\"em_resp_el\")\n",
    "el_pt_dir = os.listdir(\"em_resp_el_pt\")\n",
    "\n",
    "el_freqs = \\\n",
    "    sorted([float(filename.split(\"_\")[1]) for filename in el_dir if filename.startswith(\"freq_\")])\n",
    "el_pt_freqs = \\\n",
    "    sorted([float(filename.split(\"_\")[1]) for filename in el_pt_dir if filename.startswith(\"freq_\")])\n",
    "\n",
    "\n",
    "el_format_str = \"{:10.4f}    {:12.7f}    {:12.7f}  {:12.7f}  {:12.7f}     {:10.7f}     {:13.7f}\" \n",
    "el_line = \"# Energy (au)     sigma_xx        sigma_yy     sigma_zz     Im alpha_xx    Im alpha_yy      Im alpha_zz  \\n\"\n",
    "fd_el.write(\"\".join(el_line))\n",
    "\n",
    "el_pt_format_str = \"{:10.4f}    {:12.7f}    {:12.7f}  {:12.7f}  {:12.7f}     {:10.7f}     {:13.7f}\" \n",
    "el_pt_line = \"# Energy (au)     sigma_xx        sigma_yy     sigma_zz     Im alpha_xx    Im alpha_yy      Im alpha_zz  \\n\"\n",
    "fd_el_pt.write(\"\".join(el_line_pt))\n",
    "\n",
    "for freq in el_freqs:\n",
    "    el_freq_str = \"{:.4f}\".format(freq)  # Format freq with 4 decimal places\n",
    "    if freq == 0.0000:\n",
    "        energy = 0\n",
    "        sigma_xx = sigma_yy = sigma_zz = 0\n",
    "        Im_alpha_xx = Im_alpha_yy = Im_alpha_zz = 0\n",
    "    else:\n",
    "        cross_section_file = open(f\"el_em_resp/freq_{el_freq_str}/cross_section\", \"r\")\n",
    "        crossbits = cross_section_file.read().split()\n",
    "        energy = float(crossbits[26])\n",
    "        sigma_xx = float(crossbits[29])\n",
    "        sigma_yy = float(crossbits[33])\n",
    "        sigma_zz = float(crossbits[37])\n",
    "        Im_alpha_xx = c0 * sigma_xx / (4 * np.pi * energy)\n",
    "        Im_alpha_yy = c0 * sigma_yy / (4 * np.pi * energy)\n",
    "        Im_alpha_zz = c0 * sigma_zz / (4 * np.pi * energy)\n",
    "        cross_section_file.close()\n",
    "\n",
    "\n",
    "    el_line = [el_format_str.format(energy, sigma_xx, sigma_yy, sigma_zz,\n",
    "                                    Im_alpha_xx, Im_alpha_yy, Im_alpha_zz)]\n",
    "    el_line += [\"\\n\"]\n",
    "    fd_el.write(\" \".join(el_line))\n",
    "    fd_el.flush()\n",
    "fd_el.close()\n",
    "\n",
    "\n",
    "for freq in el_pt_freqs:\n",
    "    el_pt_freq_str = \"{:.4f}\".format(freq)  # Format freq with 4 decimal places\n",
    "    if freq == 0.0000:\n",
    "        energy = 0\n",
    "        sigma_xx = sigma_yy = sigma_zz = 0\n",
    "        Im_alpha_xx = Im_alpha_yy = Im_alpha_zz = 0\n",
    "    else:\n",
    "        cross_section_file = open(f\"el_pt_em_resp/freq_{el_freq_str}/cross_section\", \"r\")\n",
    "        crossbits = cross_section_file.read().split()\n",
    "        energy = float(crossbits[26])\n",
    "        sigma_xx = float(crossbits[29])\n",
    "        sigma_yy = float(crossbits[33])\n",
    "        sigma_zz = float(crossbits[37])\n",
    "        Im_alpha_xx = c0 * sigma_xx / (4 * np.pi * energy)\n",
    "        Im_alpha_yy = c0 * sigma_yy / (4 * np.pi * energy)\n",
    "        Im_alpha_zz = c0 * sigma_zz / (4 * np.pi * energy)\n",
    "        cross_section_file.close()\n",
    "\n",
    "\n",
    "    el_line = [el_format_str.format(energy, sigma_xx, sigma_yy, sigma_zz,\n",
    "                                    Im_alpha_xx, Im_alpha_yy, Im_alpha_zz)]\n",
    "    el_line += [\"\\n\"]\n",
    "    fd_el.write(\" \".join(el_line))\n",
    "    fd_el.flush()\n",
    "fd_el.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9e637c8f-9e8a-4328-bfa8-0740ee74f70b",
   "metadata": {},
   "source": [
    "To visualize the absortion spectra, run the following python script that computes the different components of the dipole strength function"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "14860d68-2020-4346-9325-b42b1ca4e1b8",
   "metadata": {},
   "outputs": [],
   "source": [
    "# INPUT DATA\n",
    "\n",
    "c0 = 137.0\n",
    "au_to_eV = 27.2113845\n",
    "\n",
    "\n",
    "omega_eV = omega*au_to_eV\n",
    "\n",
    "# LOAD & EXTRACT DATA\n",
    "\n",
    "data_el = np.loadtxt(\"data_computed_spectra_el.txt\", skiprows=1)\n",
    "data_el_pt = np.loadtxt(\"data_computed_spectra_el_pt.txt\", skiprows=1)\n",
    "\n",
    "el_omega = data_el[:, 0]*au_to_eV\n",
    "el_pt_omega = data_el_pt[:, 0]*au_to_eV\n",
    "\n",
    "el_S_x = (c0/(2*np.pi**2))*data_el[:, 1]\n",
    "el_S_y = (c0/(2*np.pi**2))*data_el[:, 2]\n",
    "el_S_z = (c0/(2*np.pi**2))*data_el[:, 3]\n",
    "\n",
    "el_pt_S_x = (c0/(2*np.pi**2))*data_el_pt[:, 1]\n",
    "el_pt_S_y = (c0/(2*np.pi**2))*data_el_pt[:, 2]\n",
    "el_pt_S_z = (c0/(2*np.pi**2))*data_el_pt[:, 3]\n",
    "\n",
    "    \n",
    "# PLOT RESULT\n",
    "\n",
    "gs = gridspec.GridSpec(3, 1)\n",
    "gs.update(hspace=0.1)\n",
    "fig = plt.figure(figsize=(5, 8))\n",
    "# first plot: \n",
    "ax1 = fig.add_subplot(gs[0])\n",
    "ax1.plot(el_omega, el_S_x, 'red', linestyle='-')\n",
    "ax1.plot(el_pt_omega, el_pt_S_x, 'blue', linestyle='--')\n",
    "ax1.legend([r\"electron-only\", r\"electron-photon\"],\n",
    "          loc='upper center', ncol=3, \n",
    "          bbox_to_anchor=(0.5, 1.25), prop={'size': 10})\n",
    "ax1.margins(x=0)\n",
    "ax1.xaxis.set_visible(False)\n",
    "ax1.text(0.5, 120, \"(a)\", fontsize=15)\n",
    "ax1.set_ylabel(r\"$S_{x}(\\omega)$  (a.u.)\", fontsize=15)\n",
    "ax1.get_yaxis().set_label_coords(-0.13, 0.5)\n",
    "ax1.tick_params(axis='both', which='major', labelsize=13)\n",
    "# second plot: \n",
    "ax2 = fig.add_subplot(gs[1])\n",
    "ax2.plot(el_omega, el_S_y, 'red', linestyle='-')\n",
    "ax2.plot(el_pt_omega, el_pt_S_y, 'blue', linestyle='--')\n",
    "ax2.margins(x=0)\n",
    "ax2.xaxis.set_visible(False)\n",
    "ax2.text(0.5, 120, \"(b)\", fontsize=15)\n",
    "ax2.set_ylabel(r\"$S_{y}(\\omega)$  (a.u.)\", fontsize=15)\n",
    "ax2.get_yaxis().set_label_coords(-0.13, 0.5)\n",
    "ax2.tick_params(axis='both', which='major', labelsize=13)\n",
    "# third plot: \n",
    "ax3 = fig.add_subplot(gs[2])\n",
    "ax3.plot(el_omega, el_S_z, 'red', linestyle='-')\n",
    "ax3.plot(el_pt_omega, el_pt_S_z, 'blue', linestyle='--')\n",
    "ax3.margins(x=0)\n",
    "ax3.text(0.5, 90, \"(c)\", fontsize=15)\n",
    "ax3.set_xlabel(r\"$\\hbar\\omega$ (eV)\", fontsize=15)\n",
    "ax3.set_ylabel(r\"$S_{z}(\\omega)$  (a.u.)\", fontsize=15)\n",
    "ax3.get_yaxis().set_label_coords(-0.13, 0.5)\n",
    "ax3.tick_params(axis='both', which='major', labelsize=13)\n",
    "plt.savefig('abs_spectrum_benzene.png', bbox_inches = \"tight\", dpi=200)\n",
    "plt.show()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
