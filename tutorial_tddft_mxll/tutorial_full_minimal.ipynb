{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Tutorial on full minimal coupling within the self-consistent Maxwell-TDDFT framework\n",
    "\n",
    "### Authors: Franco Bonafé, Esra Ilke Albar, Carlos Bustamante\n",
    "\n",
    "In order to account for light-matter interactions beyond the electric dipole approximation, either with an external field or with the self-induced electromagnetic fields, we need to couple real-time TDDFT with the Maxwell solver in Octopus. This can be done by using the multisystem features, and defining an electronic system plus a Maxwell system. However, if the goal is to study the beyond-dipole effects with an externally-prescribed field that is known to be a solution of Maxwell's equations (such as plane waves), then an analytical external source can be prescribed, without the propagate this incident waves in the Maxwell box. This could have several benefits, as it will be seen below.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1. XUV light-driven dynamics in benzene\n",
    "\n",
    "As beyond-dipole effects are more evident for shorter wavelengths, we focus on this tutorial on the XUV response of benzene. We will simulate the incidence of an external laser on a benzene molecule oriented in the $xy$ plane. For this tutorial we will have to consider the electronic structure of the molecule with all-electron potentials, instead of using pseudopotentials, as applying a space-dependent vector potential implies a gauge transformation that has problems (a path ambiguity) when using non-local potentials, as it is the case for pseudopotentials (see (1), (2)). For this we sill use the \"full delta\" description of the potential. \n",
    "\n",
    "We need to run first the ground state calculation uisng the following input file. For more details on the basic features of Octopus, please refer to the basic tutorials (https://www.octopus-code.org/documentation/14/tutorial/basics/)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 51,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import matplotlib.pyplot as plt\n",
    "from ase.io import read\n",
    "from postopus import Run"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 52,
   "metadata": {},
   "outputs": [],
   "source": [
    "if not os.path.exists('GS_DIR'):\n",
    "    os.mkdir('GS_DIR')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 53,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting GS_DIR/benzene.xyz\n"
     ]
    }
   ],
   "source": [
    "%%writefile GS_DIR/benzene.xyz\n",
    "12\n",
    "units: A\n",
    "      C                    0.000000    1.364239    0.000000\n",
    "      C                    1.191347    0.685530    0.000000\n",
    "      C                    1.191347   -0.685530    0.000000\n",
    "      C                    0.000000   -1.364239    0.000000\n",
    "      C                   -1.191347   -0.685530    0.000000\n",
    "      C                   -1.191347    0.685530    0.000000\n",
    "      H                    0.000000    2.441134    0.000000\n",
    "      H                    2.124739    1.238817    0.000000\n",
    "      H                    2.124739   -1.238817    0.000000\n",
    "      H                    0.000000   -2.441134    0.000000\n",
    "      H                   -2.124739   -1.238817    0.000000\n",
    "      H                   -2.124739    1.238817    0.000000"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 54,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting GS_DIR/inp\n"
     ]
    }
   ],
   "source": [
    "%%writefile GS_DIR/inp\n",
    "\n",
    "CalculationMode = gs\n",
    "ExperimentalFeatures = yes\n",
    "FromScratch = yes\n",
    "Dimensions = 3\n",
    "\n",
    "XYZCoordinates = \"benzene.xyz\"\n",
    "UnitsXYZFiles = angstrom_units\n",
    "AllElectronType = full_delta\n",
    "\n",
    "ExtraStates = 1\n",
    "\n",
    "BoxShape = parallelepiped\n",
    "Spacing = 0.1*angstrom\n",
    "%Lsize\n",
    " 3.5*angstrom | 3.5*angstrom | 1.5*angstrom\n",
    "%"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 55,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "%%bash\n",
    "cd GS_DIR\n",
    "export OMP_NUM_THREADS=1\n",
    "mpirun -np 6 octopus >& out.log"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 1.1. Dipole-level coupling\n",
    "\n",
    "We will start by calculating the system's response to the external field coupled at the dipole level, as a reference calculation. We will create a directory (`EXT_SOURCE_DIPOLE_DIR`) and create there the following input file:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 56,
   "metadata": {},
   "outputs": [],
   "source": [
    "if not os.path.exists('EXT_SOURCE_DIPOLE_DIR'):\n",
    "    os.mkdir('EXT_SOURCE_DIPOLE_DIR')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "__NOTE: replace the path in the RestartOptions variable with the proper absolute path to the GS_DIR/restart directory.__"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 57,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting EXT_SOURCE_DIPOLE_DIR/inp\n"
     ]
    }
   ],
   "source": [
    "%%writefile EXT_SOURCE_DIPOLE_DIR/inp\n",
    "\n",
    "CalculationMode = td\n",
    "ExperimentalFeatures = yes\n",
    "FromScratch = yes\n",
    "RestartWallTimePeriod = 10.01\n",
    "ParStates = no\n",
    "\n",
    "%Systems\n",
    " \"benzene\" | electronic\n",
    "%\n",
    "\n",
    "XYZCoordinates = \"benzene.xyz\"\n",
    "UnitsXYZFiles = angstrom_units\n",
    "AllElectronType = full_delta\n",
    "\n",
    "BoxShape = parallelepiped\n",
    "benzene.Spacing = 0.1*angstrom\n",
    "%benzene.Lsize\n",
    " 3.5*angstrom | 3.5*angstrom | 1.5*angstrom\n",
    "%\n",
    "\n",
    "%TDOutput\n",
    " maxwell_field\n",
    " multipoles\n",
    "%\n",
    "%Output\n",
    " density | plane_z\n",
    "%\n",
    "OutputInterval = 20\n",
    "\n",
    "omega = 270*ev\n",
    "period = 0.015*fs\n",
    "ampl = 0.0001\n",
    "tau0 = 1*period # sigma\n",
    "t0 = tau0*3\n",
    "p_s = - t0*c\n",
    "pw  = tau0*c\n",
    "phase_vec = -pi/2\n",
    "\n",
    "TDSystemPropagator = prop_aetrs\n",
    "benzene.TDTimeStep = 0.008\n",
    "TDPropagationTime = 7*period\n",
    "\n",
    "MaxwellCouplingMode = velocity_gauge_dipole\n",
    "AnalyticalExternalSource = yes\n",
    "%ExternalSource.MaxwellIncidentWaves\n",
    " plane_wave_mx_function | vector_potential | -ampl*c/omega | 0 | 0 | \"plane_waves_function\" | phase_vec\n",
    "%\n",
    "%MaxwellFunctions\n",
    "  \"plane_waves_function\" | mxf_gaussian_wave | 0 | omega/c | 0 | 0 | p_s | 0 | pw\n",
    "%"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 58,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "%%bash\n",
    "cp -r GS_DIR EXT_SOURCE_DIPOLE_DIR/benzene\n",
    "cd EXT_SOURCE_DIPOLE_DIR\n",
    "cp ../GS_DIR/benzene.xyz .\n",
    "export OMP_NUM_THREADS=1\n",
    "mpirun -np 6 octopus >& out.log"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is important to note a few differences of this input files, with respect to the non-multisystem (also known as \"legacy mode\", covered in most of the basic Octopus tutorials) input files:\n",
    "\n",
    "* `ExperimentalFeatures = yes` must be set to use the multisystem features\n",
    "* The `Systems` block has to be properly defined, accounting for all the systems present in the calculation (in this case, we are going to couple to an external plane wave prescribed by a formula, and not propagated through the Maxwell solver, therefore we only have one system)\n",
    "* The `TDSystemPropagator` option must be **mandatorily** set, otherwise the code will interpret that no propagation (also known as \"static\" propagator) wants to be used. Please note that this is a different option than the `TDPropagator` variable used in legacy-mode runs. Each system supports different propagators implemented in the new framework, e.g. the electronic system supports `prop_aetrs` and `prop_expmid`.\n",
    "* The input variables can be introduced prepending the name of the system (defining the namespace). In this case, since we have only one system (except for the `ExternalSource` one, that is not a general system), it is not necessary to do it, it's only optional (as it is shown by defining `benzene.TDTimeStep`). When we have more than one system, it is mandatory to distinguish the input options with the namespace, otherwise they will be considered to be **global** and applied to all systems.\n",
    "* The multisystem framework does not accept `TDMaxSteps` as a way to define the total simulation time (as different systems could have different time steps). Therefore we have to set `TDPropagationTime` instead.\n",
    "\n",
    "In addition to the stardard multisystem options, we define here an `ExternalSource` block, that implements the formulae for external waves in real space and real time. The relevant input variables for this are:\n",
    "\n",
    "* `AnalyticalExternalSource`: must be set to yes for the code to read the blocks prepended with `ExternalSource`.\n",
    "* `ExternalSource.MaxwellIncidentWaves` allows to define an external wave using the same input block that is used to set the boundary conditions for a Maxwell run, but in this case the defined wave will be evaluated in the whole box, at every timestep. Here we are defining a vector potential plane wave with amplitude `ampl*c/omega` polarized in the _x_ direction, defined by an envelope function called `plane_waves_function` and having a phase `phase_vec`.\n",
    "* `MaxwellFunctions`: this block allows to specify the spatio-temporal envelope function, and has been explained in the Maxwell tutorials. In this case it is a Guassian envelope which yields the following expression for the vector potential as a function of space and time:\n",
    "$ A(\\vec{r}, t) = A_0 \\exp (i(\\vec{k}\\cdot\\vec{r} - \\omega t + \\phi)) \\exp(\\frac{−(\\vec{k}\\cdot (\\vec{r} - \\vec{r}_0)/|\\vec{k}|)^2}{2 \\sigma^2})$\n",
    "* `MaxwellCouplingMode` allows to define the coupling Hamiltonian with the external field. In this case, it defines the coupling to be at dipole level in velocity gauge, using the (transverse) dipole vector potential $A(t)$, obtained from the spatial average of $A(\\vec{r},t)$:\n",
    "$H_{int} = \\frac{1}{2m} (2 \\vec{A}(t).\\hat{p} + \\vec{A}^2(t))$\n",
    "\n",
    "More information and other spatial envelope shapes can be found at the variable descriptions of `MaxwellIncidentWaves` and [MaxwellFunctions](https://www.octopus-code.org/documentation/14/variables/time-dependent/maxwellfunctions/).\n",
    "\n",
    "**Important**: in order to use the external source, one has to define the external waves including the space-dependent part, both in the carrier and in the envelope. To learn how to convert from purely time-dependent fields specified via `TDExternalFields` to external source, please follow [this tutorial](./tutorial_ext_source.ipynb).\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 1.2. Full minimal coupling\n",
    "\n",
    "Let's create the following input file in the folder `EXT_SOURCE_FULL_MINIMAL_DIR`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this case, the `MaxwellCouplingMode` specifies full minimal coupling, which can be described by the following Hamiltonian:\n",
    "\n",
    "$H_{int} = \\frac{1}{2m} (2 \\vec{A}(\\vec{r},t).\\hat{p} + \\vec{A}^2(\\vec{r},t))$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 61,
   "metadata": {},
   "outputs": [],
   "source": [
    "if not os.path.exists('EXT_SOURCE_FULL_MINIMAL_DIR'):\n",
    "    os.mkdir('EXT_SOURCE_FULL_MINIMAL_DIR')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 62,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Writing EXT_SOURCE_FULL_MINIMAL_DIR/inp\n"
     ]
    }
   ],
   "source": [
    "%%writefile EXT_SOURCE_FULL_MINIMAL_DIR/inp\n",
    "\n",
    "CalculationMode = td\n",
    "ExperimentalFeatures = yes\n",
    "FromScratch = yes\n",
    "RestartWallTimePeriod = 10.01\n",
    "ParStates = no\n",
    "\n",
    "%Systems\n",
    " \"benzene\" | electronic\n",
    "%\n",
    "\n",
    "XYZCoordinates = \"benzene.xyz\"\n",
    "UnitsXYZFiles = angstrom_units\n",
    "AllElectronType = full_delta\n",
    "\n",
    "BoxShape = parallelepiped\n",
    "benzene.Spacing = 0.1*angstrom\n",
    "%benzene.Lsize\n",
    " 3.5*angstrom | 3.5*angstrom | 1.5*angstrom\n",
    "%\n",
    "\n",
    "%TDOutput\n",
    " multipoles\n",
    "%\n",
    "%Output\n",
    " density | plane_z\n",
    "%\n",
    "OutputInterval = 20\n",
    "\n",
    "omega = 270*ev\n",
    "period = 0.015*fs\n",
    "ampl = 0.0001\n",
    "tau0 = 1*period # sigma\n",
    "t0 = tau0*3\n",
    "p_s = - t0*c\n",
    "pw  = tau0*c\n",
    "phase_vec = -pi/2\n",
    "\n",
    "TDSystemPropagator = prop_aetrs\n",
    "benzene.TDTimeStep = 0.008\n",
    "TDPropagationTime = 7*period\n",
    "\n",
    "MaxwellCouplingMode = full_minimal_coupling\n",
    "AnalyticalExternalSource = yes\n",
    "%ExternalSource.MaxwellIncidentWaves\n",
    " plane_wave_mx_function | vector_potential | -ampl*c/omega | 0 | 0 | \"plane_waves_function\" | phase_vec\n",
    "%\n",
    "%MaxwellFunctions\n",
    "  \"plane_waves_function\" | mxf_gaussian_wave | 0 | omega/c | 0 | 0 | p_s | 0 | pw\n",
    "%"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 63,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "cp -r GS_DIR EXT_SOURCE_FULL_MINIMAL_DIR/benzene\n",
    "cd EXT_SOURCE_FULL_MINIMAL_DIR\n",
    "cp ../GS_DIR/benzene.xyz .\n",
    "export OMP_NUM_THREADS=1\n",
    "mpirun -np 6 octopus >& out.log"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's see the differences between the two different coupling levels, by comparing the induced dipole moments for both runs with `ExternalSource`. We can use or adapt the following script:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "run_FULL_MINIMAL_DIR = Run(\"EXT_SOURCE_FULL_MINIMAL_DIR\") # Use Postopus to load all data produced by octopus\n",
    "run_FULL_MINIMAL_multipoles = run_FULL_MINIMAL_DIR.benzene.td.multipoles # Load multipole data\n",
    "\n",
    "run_DIPOLE_DIR = Run(\"EXT_SOURCE_DIPOLE_DIR\") # Use Postopus to load all data produced by octopus\n",
    "run_DIPOLE_DIR_multipoles = run_DIPOLE_DIR.benzene.td.multipoles # Load multipole data\n",
    "\n",
    "diff_coupling = run_DIPOLE_DIR_multipoles[\"<x>(1)\"] - run_FULL_MINIMAL_multipoles[\"<x>(1)\"] #  Calculate the difference between the two couplings\n",
    "run_FULL_MINIMAL_multipoles[\"diff_coupling\"] = diff_coupling # Add the difference to one of the existing multipole data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = run_FULL_MINIMAL_multipoles.plot(x=\"t\", y=\"<x>(1)\", label=\"Full minimal\")\n",
    "run_DIPOLE_DIR_multipoles.plot(x=\"t\", y=\"<x>(1)\", label='Dipole', ax=ax);\n",
    "run_FULL_MINIMAL_multipoles.plot(x=\"t\", y=\"diff_coupling\", ax=ax, label = \"(Full minimal - dipole) difference\");\n",
    "ax.set_ylabel('dipole moment <x> [a.u.]'); ax.set_xlabel('time [a.u.]');"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load density data with Postopus\n",
    "run_DIPOLE_density = run_DIPOLE_DIR.benzene.td.density.get_all(\"z=0\") # Load density all density data\n",
    "run_FULL_MINIMAL_density = run_FULL_MINIMAL_DIR.benzene.td.density.get_all(\"z=0\") # Load density all density data\n",
    "#Calculate the density difference\n",
    "diff_DIPOLE_density = run_DIPOLE_density - run_DIPOLE_density[0] \n",
    "diff_FULL_MINIMAL_density = run_FULL_MINIMAL_density - run_FULL_MINIMAL_density[0]\n",
    "diff_coupling_density =  diff_FULL_MINIMAL_density[:] - diff_DIPOLE_density"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Define plot function for atom positions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def plotxy_struc(ax, pos_x, pos_y):\n",
    "    return ax.scatter(pos_x, pos_y, s=100, c='k', alpha=0.5, edgecolor='k')\n",
    "\n",
    "# read atom positions\n",
    "pos = read(\"GS_DIR/benzene.xyz\").get_positions()/0.529177"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Plot the density for each case"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cmap = plt.cm.terrain\n",
    "vmax = 0.000001\n",
    "\n",
    "idxs = (10,11,12)\n",
    "fig, ax = plt.subplots(len(idxs),3,figsize=(15,12))\n",
    "\n",
    "for ii,idx in enumerate(idxs):\n",
    "\n",
    "    plots = [(diff_DIPOLE_density[idx], \"Dipole\"), \n",
    "             (diff_FULL_MINIMAL_density[idx], \"Full minimal\"), \n",
    "             (diff_coupling_density[idx], \"(Full minimal - dipole) difference\")]\n",
    "\n",
    "    for i, (data, title) in enumerate(plots):\n",
    "        data.plot(cmap=cmap, ax=ax[ii,i], x=\"x\", vmax=vmax, vmin=-vmax) \n",
    "        ax[ii,i].set_title(title)\n",
    "        plotxy_struc(ax[ii,i], pos[:,0], pos[:,1])\n",
    "\n",
    "plt.tight_layout()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 2. Self-consistent Maxwell-TDDFT of benzene with XUV excitation in full minimal coupling\n",
    "\n",
    "#### 2.1. Forward-backward dynamics in dipole coupling\n",
    "\n",
    "In addition to the beyond-dipole effects induced by the external field, there will be non-dipole effects generated by the self-induced fields, which will be in general inhomogeneous. The full currend density within the full minimal coupling, which can be calculated as the following (for non-magnetic systems):\n",
    "\n",
    "$$\n",
    "\\mathbf{J}=\\frac{\\hbar}{m} \\operatorname{Im}\\left(\\varphi^* \\nabla \\varphi\\right)+\\frac{|e|}{m c} \\mathbf{A} \\varphi^* \\varphi\n",
    "$$\n",
    "\n",
    "Now, this current is a source for electromagnetic fields, that can be propagated in time using Maxwell's equations:\n",
    "\n",
    "$$\n",
    "\\nabla \\times \\mathbf{E}=-\\frac{\\partial \\mathbf{B}}{\\partial t} \\\\\n",
    "\\nabla \\times \\mathbf{B}=\\mu_0\\left(\\mathbf{J}+\\varepsilon_0 \\frac{\\partial \\mathbf{E}}{\\partial t} \\right)\n",
    "$$\n",
    "\n",
    "As Octopus has a Maxwell solver implemented, both systems (electronic and Maxwell) can be propagated simultaneously, exchanging information every certain number of steps. The implementation in the multisystem framework allows for different time steps and different grid spacings for both systems, using interpolation to improve the representation of data in different grids.\n",
    "\n",
    "Obviously, these propagated fields can be coupled back in the TDDFT Hamiltonian, either in full minimal coupling or in dipole approximation. In the former, the code calculates the transverse vector potential from the magnetic field in Coulomb gauge, solving the Poisson equation for $\\nabla \\times \\mathbf{B}$. In the latter case (dipole coupling), the transverse electric field $\\mathbf{E}^\\perp$ or transverse vector potential $\\mathbf{A}^\\perp$ can be computed from a Helmholtz decomposition of the total fields. The longitudinal component of the electric field is already present in the Hartree potential of TDDFT, so it should not be included (4).\n",
    "\n",
    "Therefore, in this calculation we will surround the electronic box with a Maxwell box, and we will propagate both systems as they interact in a forward-backward manner. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will use the following input fuke for the calculation in dipole back-coupling:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "if not os.path.exists('MXLL_TDDFT_DIPOLE_DIR'):\n",
    "    os.mkdir('MXLL_TDDFT_DIPOLE_DIR')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile MXLL_TDDFT_DIPOLE_DIR/inp\n",
    "\n",
    "CalculationMode = td\n",
    "ExperimentalFeatures = yes\n",
    "FromScratch = yes\n",
    "Dimensions = 3\n",
    "RestartWallTimePeriod = 10.01\n",
    "ParStates = no\n",
    "\n",
    "%Systems\n",
    " \"benzene\" | electronic\n",
    " \"maxwell\" | maxwell\n",
    "%\n",
    "\n",
    "MaxwellCouplingMode = velocity_gauge_dipole\n",
    "XYZCoordinates = \"benzene.xyz\"\n",
    "UnitsXYZFiles = angstrom_units\n",
    "AllElectronType = full_delta\n",
    "\n",
    "BoxShape = parallelepiped\n",
    "benzene.Spacing = 0.1*angstrom\n",
    "%benzene.Lsize\n",
    " 3.5*angstrom | 3.5*angstrom | 1.5*angstrom\n",
    "%\n",
    "\n",
    "maxwell.BoxShape = parallelepiped\n",
    "maxwell.Spacing = 0.2*angstrom\n",
    "%Maxwell.Lsize\n",
    " 6.5*angstrom | 6.5*angstrom | 4.5*angstrom\n",
    "%\n",
    "\n",
    "%TDOutput\n",
    " maxwell_field\n",
    " multipoles\n",
    "%\n",
    "%Output\n",
    " density | plane_z\n",
    "%\n",
    "OutputInterval = 20\n",
    "\n",
    "omega = 270*ev\n",
    "period = 0.015*fs\n",
    "ampl = 0.0001\n",
    "tau0 = 1*period # sigma\n",
    "t0 = tau0*3\n",
    "p_s = - t0*c\n",
    "pw  = tau0*c\n",
    "phase_vec = -pi/2\n",
    "\n",
    "TDSystemPropagator = prop_expmid\n",
    "benzene.TDTimeStep = 0.008\n",
    "maxwell.TDTimeStep = 0.0016\n",
    "TDPropagationTime = 7*period\n",
    "InteractionTiming = timing_retarded\n",
    "\n",
    "AnalyticalExternalSource = yes\n",
    "%ExternalSource.MaxwellIncidentWaves\n",
    " plane_wave_mx_function | vector_potential | -ampl*c/omega | 0 | 0 | \"plane_waves_function\" | phase_vec\n",
    "%\n",
    "%MaxwellFunctions\n",
    "  \"plane_waves_function\" | mxf_gaussian_wave | 0 | omega/c | 0 | 0 | p_s | 0 | pw\n",
    "%\n",
    "\n",
    "MaxwellOutputInterval = 100\n",
    "%MaxwellOutput\n",
    " electric_field\n",
    " vector_potential_mag\n",
    "%\n",
    "%MaxwellBoundaryConditions\n",
    " zero | zero | zero\n",
    "%\n",
    "%MaxwellAbsorbingBoundaries\n",
    " cpml | cpml | cpml\n",
    "%\n",
    "MaxwellABWidth = 2.0*angstrom\n",
    "\n",
    "%MaxwellFieldsCoordinate\n",
    "0.0 | -4.0*angstrom  | 0.0\n",
    "0.0 | 4.0*angstrom | 0.0 \n",
    "0.0 | 0.0 | 0.0 \n",
    "%"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "cp -r GS_DIR MXLL_TDDFT_DIPOLE_DIR/benzene\n",
    "cd MXLL_TDDFT_DIPOLE_DIR\n",
    "cp ../GS_DIR/benzene.xyz .\n",
    "export OMP_NUM_THREADS=1\n",
    "mpirun -np 6 octopus >& out.log"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here we make extensive use of the namespaces, to define different grids and timesteps for the electronic and Maxwell systems. The reason to define a larger grid spacing for the Maxwell system is essentially the need to have a much larger Maxwell box (in this case, 8 angstrom per side) than the electronic box, to account for the boundary conditions, hence a larger spacing yields fewer grid points, making the calculation less expensive. The Maxwell spacing is twice as large as the electronic spacing, therefore the electronic current will be copied and rescaled to the Maxwell grid, while the Maxwell quantities will be interpolated on the electronic grid.\n",
    "\n",
    "![](img/maxwell_matter_grids_benzene.png)\n",
    "\n",
    "Also the time steps are different, with a relation 5 Maxwell steps per matter step. This implies that the current density will not always be exchanged at the exact time, as the electronic system will be \"waiting\" until the Maxwell system propagates the 5 steps. The `InteractionTiming = timing_retarded` option refers to the choice of the timig for the the exchange of quantities via the interactions. Exact timing means that the quantities are exchanged only when both systems have reached the same physical time. Retarded timing means that if one of the systems is ahead of the other, the quantities calculated from the latest time step on the system that is behind in time will be used for the interaction. This is an approximation, and the proper way would be to use a predictor-corrector scheme to account for the change of these \"lagged quantities\". However, this approximation is decent if the time steps are very small.\n",
    "\n",
    "As it is clear from the input file, the external field is still added as a (prescribed) space-dependent external source, and is not explicitly propagated in the Maxwell box. Therefore, the Maxwell system will take care only of the induced fields. The `MaxwellCouplingMode = velocity_gauge_dipole` applies for the coupling both for the external and induced fields.\n",
    "\n",
    "The options `MaxwellOutputInterval`, `MaxwellBoundaryConditions`, `MaxwellAbsorbingBoundaries` and `MaxwellABWidth` have been explained in previous Maxwell tutorials (https://www.octopus-code.org/documentation/14/tutorial/maxwell/).\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will now compare the effect on the dipole moment of the system produced by the induced fields, with respect to the previous calculation run in dipole without any backreaction. We can run a modified version of the previous script, to plot a comparison of both dipole moments, calculated with coupling at the dipole level."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "run_DIPOLE_MXLL = Run(\"MXLL_TDDFT_DIPOLE_DIR\") # Use Postopus to load all data produced by octopus\n",
    "run_DIPOLE_MXLL_multipoles = run_DIPOLE_MXLL.benzene.td.multipoles # Load multipole data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = run_DIPOLE_MXLL_multipoles.plot(x=\"t\", y=\"<x>(1)\", label=\"Maxwell+TDDFT\")\n",
    "run_DIPOLE_DIR_multipoles.plot(x=\"t\", y=\"<x>(1)\", label='TDDFT', ax=ax);\n",
    "diff = run_DIPOLE_MXLL_multipoles[\"<x>(1)\"] - run_DIPOLE_DIR_multipoles[\"<x>(1)\"]\n",
    "diff.shape, run_DIPOLE_MXLL_multipoles[\"t\"].shape, run_DIPOLE_DIR_multipoles[\"<x>(1)\"].shape\n",
    "ax.plot(run_DIPOLE_MXLL_multipoles[\"t\"], diff*100, label = \"(Maxwell+TDDFT - TDDFT) diff (x100)\")\n",
    "ax.legend(); ax.set_ylabel('dipole moment <x> [a.u.]'); ax.set_xlabel('time [a.u.]');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As it can be seen, the effect of the Maxwell coupling in dipole for this system is much smaller than the ones introduced by doing full minimal coupling, roughly 3 orders of magnitude smaller. Let's now observe the effects of backcoupling in full minimal coupling."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 2.2. Forward-backward dynamics in full minimal coupling\n",
    "\n",
    "Let's now use the full minimal coupling Hamiltonian, interacting with the external source (in \"forward\" interaction only) and with a Maxwell system (in \"forward-backward\" interaction):\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "if not os.path.exists('MXLL_TDDFT_FULL_MINIMAL_DIR'):\n",
    "    os.mkdir('MXLL_TDDFT_FULL_MINIMAL_DIR')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile MXLL_TDDFT_FULL_MINIMAL_DIR/inp\n",
    "\n",
    "CalculationMode = td\n",
    "ExperimentalFeatures = yes\n",
    "FromScratch = yes\n",
    "Dimensions = 3\n",
    "RestartWallTimePeriod = 10.01\n",
    "ParStates = no\n",
    "\n",
    "%Systems\n",
    " \"benzene\" | electronic\n",
    " \"maxwell\" | maxwell\n",
    "%\n",
    "\n",
    "MaxwellCouplingMode = full_minimal_coupling\n",
    "XYZCoordinates = \"benzene.xyz\"\n",
    "UnitsXYZFiles = angstrom_units\n",
    "AllElectronType = full_delta\n",
    "\n",
    "BoxShape = parallelepiped\n",
    "benzene.Spacing = 0.1*angstrom\n",
    "%benzene.Lsize\n",
    " 3.5*angstrom | 3.5*angstrom | 1.5*angstrom\n",
    "%\n",
    "maxwell.Spacing = 0.2*angstrom\n",
    "%Maxwell.Lsize\n",
    " 6.5*angstrom | 6.5*angstrom | 4.5*angstrom\n",
    "%\n",
    "\n",
    "%TDOutput\n",
    " multipoles\n",
    " energy\n",
    "%\n",
    "\n",
    "omega = 270*ev\n",
    "period = 0.015*fs\n",
    "ampl = 0.0001\n",
    "tau0 = 1*period # sigma\n",
    "t0 = tau0*3\n",
    "p_s = - t0*c\n",
    "pw  = tau0*c\n",
    "phase_vec = -pi/2\n",
    "\n",
    "TDSystemPropagator = prop_expmid\n",
    "benzene.TDTimeStep = 0.008\n",
    "maxwell.TDTimeStep = 0.0016\n",
    "TDPropagationTime = 7*period\n",
    "InteractionTiming = timing_retarded\n",
    "\n",
    "AnalyticalExternalSource = yes\n",
    "%ExternalSource.MaxwellIncidentWaves\n",
    " plane_wave_mx_function | vector_potential | -ampl*c/omega | 0 | 0 | \"plane_waves_function\" | phase_vec\n",
    "%\n",
    "%MaxwellFunctions\n",
    "  \"plane_waves_function\" | mxf_gaussian_wave | 0 | omega/c | 0 | 0 | p_s | 0 | pw\n",
    "%\n",
    "OutputInterval = 20\n",
    "OutputFormat = plane_z + plane_x + plane_y + axis_y\n",
    "%Output\n",
    " density\n",
    "%\n",
    "\n",
    "MaxwellOutputInterval = 100\n",
    "%MaxwellOutput\n",
    " vector_potential_mag\n",
    "%\n",
    "%MaxwellBoundaryConditions\n",
    " zero | zero | zero\n",
    "%\n",
    "%MaxwellAbsorbingBoundaries\n",
    " cpml | cpml | cpml\n",
    "%\n",
    "MaxwellABWidth = 2.0*angstrom\n",
    "\n",
    "%MaxwellFieldsCoordinate\n",
    "0.0 | -4.0*angstrom  | 0.0\n",
    "0.0 | 4.0*angstrom | 0.0 \n",
    "0.0 | 0.0 | 0.0 \n",
    "%"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "cp -r GS_DIR MXLL_TDDFT_FULL_MINIMAL_DIR/benzene\n",
    "cd MXLL_TDDFT_FULL_MINIMAL_DIR\n",
    "cp ../GS_DIR/benzene.xyz .\n",
    "export OMP_NUM_THREADS=1\n",
    "mpirun -np 6 octopus >& out.log"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's see the difference between the dipole moments calculated with the external source only, and the one including the Maxwell box:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "run_FULLMIN_MXLL = Run(\"MXLL_TDDFT_FULL_MINIMAL_DIR/\") # Use Postopus to load all data produced by octopus\n",
    "run_FULLMIN_MXLL_multipoles = run_FULLMIN_MXLL.benzene.td.multipoles # Load multipole data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = run_FULLMIN_MXLL_multipoles.plot(x=\"t\", y=\"<x>(1)\", label=\"Maxwell+TDDFT\")\n",
    "run_FULL_MINIMAL_multipoles.plot(x=\"t\", y=\"<x>(1)\", label='TDDFT', ax=ax);\n",
    "diff = run_FULLMIN_MXLL_multipoles[\"<x>(1)\"] - run_FULL_MINIMAL_multipoles[\"<x>(1)\"]\n",
    "ax.plot(run_FULLMIN_MXLL_multipoles[\"t\"], diff*100, label = \"(Maxwell+TDDFT - TDDFT) diff (x100)\")\n",
    "ax.legend(); ax.set_ylabel('dipole moment <x> [a.u.]'); ax.set_xlabel('time [a.u.]');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The difference in the dipole moments can be explained as a small frequency shift induced by the self-consistent Maxwell coupling, as it was observed for plasmonic dimers (4). "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load density data with Postopus\n",
    "run_FULLMIN_MXLL_density = run_FULLMIN_MXLL.benzene.td.density.get_all(\"z=0\") # Load density all density data\n",
    "run_FULLMIN_MXLL_vecpot_x = run_FULLMIN_MXLL.maxwell.td.vector_potential_mag.x.get_all(\"z=0\")\n",
    "run_FULLMIN_MXLL_vecpot_y = run_FULLMIN_MXLL.maxwell.td.vector_potential_mag.y.get_all(\"z=0\")\n",
    "#Calculate the density difference\n",
    "diff_FULLMIN_MXLL_density = run_FULLMIN_MXLL_density - run_FULLMIN_MXLL_density[0]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, it is interesting to see the magnitud and space distribution of the induced vector potential.  The plot produced for several steps after the external field is off, is the following:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cmap = plt.cm.terrain\n",
    "vmax_dens = 1e-6\n",
    "vmax_vecpot = 1e-8\n",
    "\n",
    "idxs = (10,11,12,13)\n",
    "fig, ax = plt.subplots(len(idxs),3,figsize=(15,4*len(idxs)))\n",
    "\n",
    "for ii,idx in enumerate(idxs):\n",
    "\n",
    "    plots = [(diff_FULLMIN_MXLL_density[idx], \"Full minimal Density Diff.\", vmax_dens), \n",
    "             (run_FULLMIN_MXLL_vecpot_x[idx], \"$A_x$\", vmax_vecpot), \n",
    "             (run_FULLMIN_MXLL_vecpot_y[idx], \"$A_y$\", vmax_vecpot)]\n",
    "\n",
    "    for i, (data, title, vmaxx) in enumerate(plots):\n",
    "        data.plot(cmap=cmap, ax=ax[ii,i], x=\"x\", vmax=vmaxx, vmin=-vmaxx) \n",
    "        ax[ii,i].set_title(title)\n",
    "        plotxy_struc(ax[ii,i], pos[:,0], pos[:,1])\n",
    "        ax[ii,i].set_xlim(-7,7); ax[ii,i].set_ylim(-7,7);\n",
    "\n",
    "plt.tight_layout()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "It can be seen that the maxima and minima of the vector potential are dephase by a quarter of a cycle with respect to the maxima and minima of the induced dipole, and there is still a contribution even after the external drive is switched off. However, its magnitud is much smaller than the external one, for this particular system.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Bonus exercises:\n",
    "\n",
    "1. Re run the calculations with the incident waves propagating along the $z$ direction (perpendicular to the plane of the molecule). What changes do you expect? What do you actually obtain?\n",
    "\n",
    "2. Change the external field to be circularly polarized. Would the results with forward-backward interaction change substantially?\n",
    "\n",
    "3. Include a calculation with `MaxwellCouplingMode = multipolar_expansion` including the electric dipole, electric quadrupole and magnetic dipole terms (in forward-coupling only). Keep in mind that in this case, the external source has to be defined as an electric field (to get equivalent results, $E_0 = -A_0*\\omega/c$ and the `phase_vec = 0`). How does it compare with the full minimal coupling results?\n",
    "\n",
    "4. Now displace the molecule off-center and recompute the response of the electric quadrupole and the magnetic dipole, at the electric dipole and full minimal coupling levels. What level of theory makes the results origin-dependent?\n",
    "\n",
    "----"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### References\n",
    "\n",
    " \n",
    "(1) Ismail-Beigi, S., Chang, E. K., & Louie, S. G. (2001). Coupling of nonlocal potentials to electromagnetic fields. Physical Review Letters, 87(8), 87402-1-87402–87404. https://doi.org/10.1103/PhysRevLett.87.087402\n",
    "\n",
    "(2) Pickard, C. J., & Mauri, F. (2003). Nonlocal pseudopotentials and magnetic fields. Physical Review Letters, 91(19), 7–10. https://doi.org/10.1103/PhysRevLett.91.196401\n",
    "\n",
    "(3) Jensen, S. V. B., Lund, M. M., & Madsen, L. B. (2020). Nondipole strong-field-approximation Hamiltonian. Physical Review A, 101(4), 1–14. https://doi.org/10.1103/PhysRevA.101.043408\n",
    "\n",
    "(4) Jestädt, R., Ruggenthaler, M., Oliveira, M. J. T., Rubio, A., & Appel, H. (2019). Light-matter interactions within the Ehrenfest–Maxwell–Pauli–Kohn–Sham framework: fundamentals, implementation, and nano-optical applications. Advances in Physics, 68(4), 225–333. https://doi.org/10.1080/00018732.2019.1695875\n",
    "\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
